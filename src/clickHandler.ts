namespace Test {
    export const clickHandler = (e: Event) => {
        console.log(`I've been clicked! My tagName is ${(e.target as HTMLElement).tagName}`);
    }
}
