/* eslint-disable */
module.exports = function (grunt) {
    // 1. All configuration goes here
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        jasmine: {
            coverage: {
                src: ['build/src/clickHandler.js'],
                options: {
                    specs: ['build/spec/*Spec.js', 'spec/*Spec.js'],
                    helpers: 'spec/helpers/*.js',
                },
            },
        },

        ts: {
            src: {
                src: ['src/**/*.ts'],
                dest: 'build/',
                options: {
                    rootDir: 'src',
                    target: 'es3',
                    sourceMap: true,
                    declaration: true
                }
            },
            test: {
                src: ['spec/**/*.ts'],
                dest: 'build/',
                options: {
                    rootDir: './',
                    target: 'es3',
                    sourceMap: true,
                    declaration: true
                }
            },
        },
    });

    // 3. Where we tell Grunt we plan to use this plug-in.
    grunt.loadNpmTasks('grunt-ts');

    grunt.loadNpmTasks('grunt-contrib-jasmine');

    // 4. Where we tell Grunt what to do when we type "grunt" into the terminal.
    grunt.registerTask('test',
        [
            'ts',
            'jasmine',
        ]);
};
