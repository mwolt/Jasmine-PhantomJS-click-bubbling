describe('This should work from JavaScript', function () {
    var parentElement;
    var button;
    var mouseEvent;

    var clickHandler = function (e) {
        console.log("I've been clicked! My tagName is " + e.target.tagName);
    };

    var interceptor = function (e) {
        mouseEvent = e;
    };

    beforeEach(function () {
        parentElement = document.createElement('div');
        button = document.createElement('button');
        parentElement.appendChild(button);
    });

    it('click()ing the button should trigger the function', function () {
        parentElement.addEventListener('click', clickHandler);
        spyOn(console, 'log').and.callThrough();

        button.click();

        expect(console.log).toHaveBeenCalled();
    });

    it('dispatching a mouseup event on the button should trigger the function', function () {
        parentElement.addEventListener('mouseup', clickHandler);
        spyOn(console, 'log').and.callThrough();

        button.dispatchEvent(new Event('mouseup', { bubbles: true, cancelable: true }));

        expect(console.log).toHaveBeenCalled();
    });

});
