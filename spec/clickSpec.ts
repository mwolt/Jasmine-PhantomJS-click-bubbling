/// <reference path="../src/clickHandler.ts" />
describe('This should work', () => {
    let parentElement: HTMLDivElement;
    let button: HTMLButtonElement;

    beforeEach(() => {
        parentElement = document.createElement('div');
        button = document.createElement('button');
        parentElement.appendChild(button);
    });

    it('click()ing the button should trigger the function', () => {
        parentElement.addEventListener('click', Test.clickHandler);
        spyOn(console, 'log').and.callThrough();

        button.click();

        expect(console.log).toHaveBeenCalled();
    });

    it('dispatching a mouseup event on the button should trigger the function', () => {
        parentElement.addEventListener('mouseup', Test.clickHandler);
        spyOn(console, 'log').and.callThrough();

        button.dispatchEvent(new MouseEvent('mouseup', { bubbles: true, cancelable: true }));

        expect(console.log).toHaveBeenCalled();
    });

    it('dispatching a click event on the button should trigger the function', () => {
        parentElement.addEventListener('click', Test.clickHandler);
        spyOn(console, 'log').and.callThrough();

        button.dispatchEvent(new Event('click', {'bubbles': true}));

        expect(console.log).toHaveBeenCalled();
    });

    it('dispatching a click event on the button should trigger the function', () => {
        parentElement.addEventListener('click', Test.clickHandler);
        spyOn(console, 'log').and.callThrough();

        button.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true }));

        expect(console.log).toHaveBeenCalled();
    });
});
